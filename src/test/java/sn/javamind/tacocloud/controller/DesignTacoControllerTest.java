package sn.javamind.tacocloud.controller;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import sn.javamind.tacocloud.model.Ingredient;
import sn.javamind.tacocloud.model.Taco;
import sn.javamind.tacocloud.repository.IngredientRepository;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(DesignTacoController.class)
class DesignTacoControllerTest {

    @MockBean
    private IngredientRepository ingredientRepository;

    private Taco design;
    private List<Ingredient> ingredients;
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        ingredients = Arrays.asList(
                new Ingredient("FLTO", "Flour Tortilla", Ingredient.Type.WRAP),
                new Ingredient("CARN", "Carnitas", Ingredient.Type.PROTEIN),
                new Ingredient("TMTO", "Diced Tomatoes", Ingredient.Type.VEGGIES),
                new Ingredient("CHED", "Cheddar", Ingredient.Type.CHEESE),
                new Ingredient("SRCR", "Sour Cream", Ingredient.Type.SAUCE)
        );
        List<String> ingredientNames = ingredients.stream()
                .map(Ingredient::getName)
                .collect(Collectors.toList());

        design = Taco.builder()
                .name("New Taco")
                .ingredients(ingredientNames)
                .build();
    }

    @Test
    public void testShowDesignForm() throws Exception {
        mockMvc.perform(get("/design"))
                .andExpect(status().isOk())
                .andExpect(view().name("design"))
                .andExpect(content().string(Matchers.containsString("Design your taco!")));
    }

    @Test
    public void processDesign_shouldReturnDesignFormViewIfFormIsInvalid() throws Exception {
        mockMvc.perform(post("/design"))
                .andExpect(view().name("design"));
    }

    @Test
    public void processDesign_shouldRedirectIfDesignIsValid() throws Exception {
        mockMvc.perform(
                post("/design")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("name", design.getName()))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/orders/current"));
    }
}
